InboxSDK.loadScript('https://cdn.jsdelivr.net/npm/vue@2.5.17/dist/vue.js');
InboxSDK.loadScript('https://cdn.jsdelivr.net/npm/axios/dist/axios.min.js');

const INBOX_SDK_APP_ID = 'sdk_marioChallenge_e758935a7d'; // '<YOUR_APP_ID>';

var ID = function () {
  return '_' + Math.random().toString(36).substr(2, 9);
};

InboxSDK.load(2, INBOX_SDK_APP_ID).then(function (sdk) {
  sdk.Compose.registerComposeViewHandler(function (composeView) {
    composeView.addButton({
      title: 'Personal Snnipets',
      iconUrl: chrome.extension.getURL('images/button_icon.png'),
      onClick: function (event) {
        const modalRef = sdk.Widgets.showModalView({
          title: 'Personal Snnipets',
          el: `<div id="personal-snnipets-template"></div>`,
        });

        new Vue({
          el: '#personal-snnipets-template',
          template: `
              <div>
                <template v-if="snippets.length">
                    <table class="snippets">
                        <thead>
                            <tr>
                                <th scope="col">Name</th>
                                <th scope="col">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr v-for="snippet in snippets" :key="snippet.index">
                                <td data-label="Name">{{ snippet.name }}</td>
                                <td>
                                    <button
                                        type="button" 
                                        v-on:click="setSnippet(snippet.text)"
                                        class="btn btn-default"
                                    >
                                        Use
                                    </button>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </template>
                <div v-else>
                    No snippets, add one and will be see here.
                </div>
            </div>
          `,

          data() {
            return {
              snippets: [],
              showForm: false,
            };
          },
          methods: {
            setSnippet(text) {
              composeView.insertTextIntoBodyAtCursor(text);
              modalRef.close();
            }
          },
          created() {
              axios
                .get(`https://challenge.marioidival.xyz/snippets`)
                .then((response) => {
                    if (response.status == 200) {
                        this.snippets = response.data.snippets;
                    }
                });
          },
        });
      },
    });
  });
});
